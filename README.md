## Summary
This is a java RFID reader wrapper that connect to 
a UHF RFID reader and read all the nearby RFID labels

```
READER_IP=192.168.0.103
READER_PORT=7776
HOST_IP=169.254.67.98
HOST_PORT=6000
ANTENNA_PORT=1
```