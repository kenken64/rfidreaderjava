package sg.massiveinfinity.rfid;

import Reader.ReaderAPI;
import io.github.cdimascio.dotenv.Dotenv;
import java.util.NoSuchElementException;

import sdk.Utility;

/**
 *
 * @author kenken64
 */
public class MainHostReader {
  
  public static void main(String[] args){
      try{
        System.out.println("RFID console reader..");
        new RFIDReaderTimer().start();

      }catch (NoSuchElementException e) {
        System.out.println("CTRL C Program stops");
        ReaderAPI.Net_DisconnectScanner();    
      }
  }
}

class RFIDReaderTimer extends Thread
{
    private boolean endtime=false;
    private boolean run=false;
    private int iInterval = 1000;

    private byte[] ReaderID = new byte[10];
    int Read_times=0;
    int hScanner[] = new int[1];
    int mem=0;
    int ptr=0;
    int len=0;
    int EPC_Word=0; 
    byte[] mask = new byte[512];
    byte[][] EPCC1G2_IDBuffer = new byte[ReaderAPI.MAX_LABELS][ReaderAPI.ID_MAX_SIZE_96BIT];
    private static String readerIp = null;
    private static String readerPort = null;
    private static String hostIp = null;
    private static String hostPort = null;
    private static String antennaPort = null;
    int res = -1;
    
    @Override
    public void run()
    {
        Dotenv dotenv = Dotenv.load();
        readerIp = dotenv.get("READER_IP");
        readerPort = dotenv.get("READER_PORT");
        hostIp = dotenv.get("HOST_IP");
        hostPort = dotenv.get("HOST_PORT");
        antennaPort = dotenv.get("ANTENNA_PORT");
        System.out.println(readerIp);
        System.out.println(readerPort);
        System.out.println(hostIp);
        System.out.println(hostPort);
        //iRet = ReaderAPI.Net_ConnectScanner(hScanner, "192.168.0.103", 1969, "192.168.0.71", 5555);
        res = ReaderAPI.Net_ConnectScanner(hScanner, readerIp, Integer.parseInt(readerPort), hostIp, Integer.parseInt(hostPort));
        
         if (res==ReaderAPI._OK){
            // keep looing to get data from reader
            for(;;)
            {
                try{
                    int be_antenna	=	0;
                    int i,j,k,ID_len=0,ID_len_temp=0;
                    String str = "",str_temp;
                    String strTemp;
                    byte[] temp = new byte[64*2];
                    byte[] DB = new byte[128];
                    byte[] IDBuffer = new byte[30*256];
                    int[] nCounter = new int[2];

                    res=ReaderAPI.Net_SetAntenna(hScanner[0], Integer.parseInt(antennaPort));

                    if ( ReaderAPI._OK != res )
                    {
                        // error
                        return;
                    }

                    res=ReaderAPI.Net_EPC1G2_ReadLabelID(hScanner[0],mem,ptr,len,mask,IDBuffer,nCounter);

                    if (res==ReaderAPI._OK)
                    {
                        if ( nCounter[0] > 8 ){
                            i = nCounter[0];
                        }

                        for(i=0;i<nCounter[0];i++){
                            if (IDBuffer[ID_len]>32)
                            {
                                    nCounter[0]=0;
                                    break;
                            }
                            ID_len_temp=IDBuffer[ID_len]*2+1;//1word=16bit
                            //memcpy(EPCC1G2_IDBuffer[i], &IDBuffer[ID_len], ID_len_temp);
                            System.arraycopy(IDBuffer, ID_len, EPCC1G2_IDBuffer[i], 0, ID_len_temp);
                            ID_len+=ID_len_temp;
                        }

                        if (nCounter[0]>0){
                            System.out.println(nCounter[0]);
                            System.out.println(nCounter[0]>0);
                        }

                        for(i=0;i<nCounter[0];i++){
                            str="";
                            ID_len=EPCC1G2_IDBuffer[i][0]*2;
                            System.arraycopy(EPCC1G2_IDBuffer[i], 1, temp, 0, ID_len);

                            str = Utility.bytes2HexString(temp, ID_len);
                            System.out.println("EPCC[start]=" + str + "\n");
                        }
                    }

                    if (endtime){
                        run = false;
                        break;
                    }

                    Thread.sleep(iInterval);
                }
                catch (InterruptedException ex)
                {
                    System.out.println(ex);
                }
                run = true;
            }
         }else{
             System.out.println("Failed to connect to the RFID reader!");
         }
        
    }
    public void setEnd(boolean t)
    {
        endtime=t;
    }
    public void setInterval(int time)
    {
        iInterval=time;
    }
    public boolean getRun()
    {
        return run;
    }
}



